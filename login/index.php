<!DOCTYPE html>
<?php
if(isset($_COOKIE["admin"])){
    header("location: http://192.168.1.160/topicos/inicio/");
}
?><html lang="es">
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/icono.png" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/login.css"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>
</head>

<body class="full-cover-background">
<div class="bg-image"></div>
<div class="form-container">

    <p class="text-center" style="margin-top: 17px;">

        <img src="../assets/img/admin.jfif" width="100" height="100">
    </p>

    <h4 class="text-center all-tittles" style="margin-bottom: 30px;">inicia sesión</h4>

    <form action=".." id="login"">
        <div class="group-material-login">
            <input type="text" class="material-login-control" id="mail" required maxlength="70">
            <span class="highlight-login"></span>
            <span class="bar-login"></span>
            <label><i class="zmdi zmdi-account"></i> &nbsp; Usuario</label>
        </div><br>
        <div class="group-material-login">
            <input type="password" class="material-login-control" id="pass" required maxlength="70">
            <span class="highlight-login"></span>
            <span class="bar-login"></span>
            <label><i class="zmdi zmdi-lock"></i> &nbsp; Contraseña</label>
        </div>
        <div>
            <label><input type="checkbox" id="remember"> Recuerdame</label>
            <button class="btn-login" id="ingresar" type="submit" onclick="enviar()">Ingresar &nbsp; <i class="zmdi zmdi-arrow-right"></i></button>
        </div>
    </form>
    </form>
</div>

</body>
<script>
    function enviar(){
        var mail=$("#mail").val();
        var pass=$("#pass").val();
        var coco=document.getElementById("remember").checked;
        var request = {
            "url": "http://192.168.1.160/topicos/controladores/APIUsuarioController",
            "method": "POST",
            "timeout": 100000000,
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            "data": {
                "email": mail,
                "pass": pass,
                "opcion": "loginAdmin"
            }
        };

        $.ajax(request).done(function (response) {
            if(response=="password"){
                alert("La contraeña es erronea, verifique e intente nuevamente.");
            }
            else{
                if(response=="usuario") {
                    alert("No se encuentra el administrador, verifique e intente nuevamente.");
                }
                else{
                    var time = Date.now();
                    time = time + 24*365*3600*1000;
                    var d = new Date(time);
                    var expires = "; expires="+ d.toUTCString();
                    if (!coco) {
                        expires = "";
                    }
                    var content = JSON.parse(response);
                    document.cookie = "admin="+content['ID']+expires+"; path=/";
                    document.cookie = "nombre="+content['nombre']+expires+"; path=/";
                    window.location.replace("http://192.168.1.160/topicos/inicio");
                }
            }
            $("#login").submit();
        });
    }
</script>
</html>