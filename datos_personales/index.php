
<!DOCTYPE html>
<?php
if(!isset($_COOKIE["admin"])){
    header("location: http://192.168.1.160/topicos/login/");
}

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://192.168.1.160/topicos/controladores/APIUsuarioController',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => 'opcion=datos_personales&id='.$_GET['id'],
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/x-www-form-urlencoded'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$datos=json_decode($response,true);
echo $response;
?>
<html lang="es">
<head>
    <title>Usuarios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/icono.png" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>
    <link href="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
    <script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table-locale-all.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/extensions/export/bootstrap-table-export.min.js"></script>

</head>
<body>
<?php
include("../componentes/navbar.php");
?>
    <div class="content-page-container full-reset custom-scroll-containers">
        <?php
        include("../componentes/header.php");
        ?>
        <center><table border="1" style="margin-top: 10px;">
            <th colspan="3"><center>DATOS PERSONALES</center></th>
            <tr>
                <td rowspan="7"><img style="width: 250px; height: 250px;" src="data:image/png;base64, <?php echo $datos['usuario']['FP']?>"/></td>
                <td style="font-weight: bold;">Nombre</td>
                <td><?php echo $datos['persona']['nombre']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Apellido Paterno</td>
                <td><?php echo $datos['persona']['AP']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Apellido Materno</td>
                <td><?php echo $datos['persona']['AM']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Carnet de Identidad</td>
                <td><input type="hidden" id="ci" value="<?php echo $datos['persona']['ci']; ?>"><?php echo $datos['persona']['ci']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Fecha de Nacimiento</td>
                <td><?php echo date('d/m/Y',strtotime($datos['usuario']['FN'])); ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Telefono</td>
                <td><?php echo $datos['usuario']['telefono']; ?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Localidad</td>
                <td><?php echo $datos['localidad'][0].", ".$datos['localidad'][1].", ".$datos['localidad'][2].", ".$datos['localidad'][3]; ?></td>
            </tr>
            <th colspan="3"><center>CUENTA</center></th>
            <tr>
                <td colspan="2" style="font-weight: bold;">Fecha de Registro</td>
                <td><?php echo date('d/m/Y',strtotime($datos['usuario']['FR'])); ?></td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: bold;">Tipo de cuenta</td>
                <td><?php echo $datos['usuario']['tipo']; ?></td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: bold;">Tipo de cuenta</td>
                <td><?php echo $datos['cuenta']['email']; ?></td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: bold;">Tipo de cuenta</td>
                <td><?php echo $datos['cuenta']['estado']; ?></td>
            </tr>
            <?php if($datos['usuario']['FA']!=""&&$datos['usuario']['FC']!=""&&$datos['usuario']['FRC']!="") {?>
            <th colspan="3"><center>IMAGENES PARA VERIFICAR</center></th>
            <tr>
                <td><img style="width: 200px; height: 200px;" src="data:image/png;base64, <?php echo $datos['usuario']['FA']?>" /></td>
                <td><img style="width: 200px; height: 200px;" src="data:image/png;base64, <?php echo $datos['usuario']['FC']?>" /></td>
                <td><img style="width: 200px; height: 200px;" src="data:image/png;base64, <?php echo $datos['usuario']['FRC']?>"/></td>
            </tr>
            <?php } ?>
        </table></center>
        <?php if($datos['cuenta']['estado']=="Pendiente") {?>
        <center><div>
            <input type="button" onclick="aprobar(true)" value="Aprobar">
            <input type="button" onclick="aprobar(false)" value="Rechazar">
        </div></center>
        <?php } ?>
    </div>
</body>
<script>
    function aprobar(aceptado){
        var ci=$("#ci").val();
        var settings = {
            "url": "http://192.168.1.160/topicos/controladores/APIUsuarioController",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            "data": {
                "opcion": "aprobar",
                "id": ci,
                "aceptado":aceptado
            }
        };

        $.ajax(settings).done();
        location.reload();
    }
</script>
</html>