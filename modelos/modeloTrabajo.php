<?php
include "conexion.php";

class modeloTrabajo{

    var $conexion;

    function __construct()
    {
        $this->conexion = new conexion();
    }

    function oferta(){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT empleado.*,persona.* FROM empleado,persona WHERE empleado.cipersona=persona.ci");
        $ofertas=array();
        while($fila=mysqli_fetch_array($consulta)){
            $oferta=array("ci"=>$fila['ci'],"nombre"=>$fila["nombre"]." ".$fila["apellidop"]." ".$fila["apellidom"], "dir"=>$fila["dir"], "CP"=>$fila['calificacionpromedio'], "telefono"=>$fila['telefono']);
            $consulta1=$this->conexion->consulta("SELECT * FROM localidad WHERE id=".$fila["idlocalidad"]);
            $fila1=mysqli_fetch_array($consulta1);
            $oferta["ciudad"]=$fila1["nombre"];
            $consulta2=$this->conexion->consulta("SELECT * FROM localidad WHERE id=".$fila1["idlocalidad"]);
            $fila2=mysqli_fetch_array($consulta2);
            $oferta["region"]=$fila2["nombre"];
            $consulta3=$this->conexion->consulta("SELECT * FROM localidad WHERE id=".$fila2["idlocalidad"]);
            $fila3=mysqli_fetch_array($consulta3);
            $oferta["estado"]=$fila3["nombre"];
            $consulta4=$this->conexion->consulta("SELECT * FROM localidad WHERE id=".$fila3["idlocalidad"]);
            $fila4=mysqli_fetch_array($consulta4);
            $oferta["pais"]=$fila4["nombre"];
            $consulta5=$this->conexion->consulta("SELECT empleadooficio.*,oficio.* FROM empleadooficio,oficio WHERE empleadooficio.idempleado=".$fila['ci']." AND empleadooficio.idoficio=oficio.id");
            $oferta["oficios"]=array();
            while($fila5=mysqli_fetch_array($consulta5)){
                $consulta6=$this->conexion->consulta("SELECT * FROM categoriaoficio WHERE id=".$fila5["idcategoriaoficio"]);
                $fila6=mysqli_fetch_array($consulta6);
                $consulta7=$this->conexion->consulta("SELECT * FROM categoriaoficio WHERE id=".$fila6["idcategoriaoficio"]);
                $fila7=mysqli_fetch_array($consulta7);
                array_push($oferta["oficios"],array("ID"=>$fila5["idoficio"],"HI"=>$fila5["horarioI"],"HF"=>$fila5["horarioF"], "nombre"=>$fila5["nombre"], "subcatID"=>$fila6["id"], "subcat"=>$fila6["nombre"], "padreID"=>$fila7["id"], "padre"=>$fila7["nombre"]));
            }
            array_push($ofertas,$oferta);
        }
        $this->conexion->desconectar();
        return $ofertas;
    }
}
?>