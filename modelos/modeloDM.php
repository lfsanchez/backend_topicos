<?php
include "conexion.php";

class modeloDM{

    var $conexion;

    function __construct()
    {
        $this->conexion = new conexion();
    }

    function buscarCatOf(){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT * FROM categoriaoficio");
        $this->conexion->desconectar();
        $filas = array();
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("ID"=>$fila[0], "nombre"=>$fila[1], "idpadre"=>$fila[2]);
            array_push($filas,$f);
        }
        return $filas;
    }
    function buscarOf(){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT * FROM categoriaoficio WHERE id NOT IN (SELECT idcategoriaoficio FROM categoriaoficio WHERE idcategoriaoficio IS NOT NULL)");
        $this->conexion->desconectar();
        $filas = array();
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("ID"=>$fila[0], "nombre"=>$fila[1], "idpadre"=>$fila[2]);
            array_push($filas,$f);
        }
        return $filas;
    }
    function buscarLoc(){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT * FROM localidad");
        $this->conexion->desconectar();
        $filas = array();
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("ID"=>$fila[0], "nombre"=>$fila["nombre"], "idpadre"=>$fila[2]);
            array_push($filas,$f);
        }
        return $filas;
    }

    function Listar(){
        $this->conexion->conectar();
        $consulta1=$this->conexion->consulta("SELECT * FROM categoriaoficio");
        $filas = array();
        while($fila=mysqli_fetch_array($consulta1)){
            if(count($fila)>0){
                if(isset($fila[2])){
                    $consulta2=$this->conexion->consulta("SELECT * FROM categoriaoficio WHERE id=$fila[2]");
                    while($fila1=mysqli_fetch_array($consulta2)){
                        $f = array("ID"=>$fila[0], "nombre"=>$fila[1], "idcat"=>1, "categoria"=>"Categoria Oficio", "idpadre"=>$fila[2], "padre"=>$fila1[1]);
                    }
                }
                else{
                    $f = array("ID"=>$fila[0], "nombre"=>$fila[1], "idcat"=>1, "categoria"=>"Categoria Oficio", "idpadre"=>$fila[2], "padre"=>"");
                }
                array_push($filas,$f);
            }
        }
        $consulta1=$this->conexion->consulta("SELECT * FROM oficio");
        while($fila=mysqli_fetch_array($consulta1)){
            if(count($fila)>0){
                if(isset($fila[2])){
                    $consulta2=$this->conexion->consulta("SELECT * FROM categoriaoficio WHERE id=$fila[2]");
                    while($fila1=mysqli_fetch_array($consulta2)){
                        $f = array("ID"=>$fila[0], "nombre"=>$fila[1], "idcat"=>2, "categoria"=>"Oficio", "idpadre"=>$fila[2], "padre"=>$fila1[1]);
                    }
                }
                else{
                    $f = array("ID"=>$fila[0], "nombre"=>$fila[1], "idcat"=>2, "categoria"=>"Oficio", "idpadre"=>$fila[2], "padre"=>"");
                }
                array_push($filas,$f);
            }
        }
        $consulta1=$this->conexion->consulta("SELECT * FROM localidad");
        while($fila=mysqli_fetch_array($consulta1)){
            if(count($fila)>0){
                if(isset($fila[2])){
                    $consulta2=$this->conexion->consulta("SELECT * FROM localidad WHERE id=$fila[2]");
                    while($fila1=mysqli_fetch_array($consulta2)){
                        $f = array("ID"=>$fila[0], "nombre"=>$fila[1], "idcat"=>3, "categoria"=>"Localidad", "idpadre"=>$fila[2], "padre"=>$fila1[1]);
                    }
                }
                else{
                    $f = array("ID"=>$fila[0], "nombre"=>$fila[1], "idcat"=>3, "categoria"=>"Localidad", "idpadre"=>$fila[2], "padre"=>"");
                }
                array_push($filas,$f);
            }
        }
        $this->conexion->desconectar();
        $array=["rows"=>$filas];
        return $array;
    }

    function eliminarCatOf($id){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("DELETE FROM categoriaoficio WHERE id=$id");
        $this->conexion->desconectar();
    }

    function eliminarOf($id){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("DELETE FROM oficio WHERE id=$id");
        $this->conexion->desconectar();
    }

    function eliminarLoc($id){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("DELETE FROM localidad WHERE id=$id");
        $this->conexion->desconectar();
    }

    function guardarCatOf($nombre,$idpadre){
        $this->conexion->conectar();
        if($idpadre!=""){
            $this->conexion->consulta("INSERT INTO categoriaoficio VALUES(NULL,'$nombre',$idpadre)");
        }
        else{
            $this->conexion->consulta("INSERT INTO categoriaoficio VALUES(NULL,'$nombre',NULL)");
        }
        $this->conexion->desconectar();
    }

    function guardarOf($nombre, $idpadre){
        $this->conexion->conectar();
        if($idpadre!=""){
            $this->conexion->consulta("INSERT INTO oficio VALUES(NULL,'$nombre',$idpadre)");
        }
        else{
            $this->conexion->consulta("INSERT INTO oficio VALUES(NULL,'$nombre',0)");
        }
        $this->conexion->desconectar();
    }

    function guardarLoc($nombre, $idpadre){
        $this->conexion->conectar();
        if($idpadre!=""){
            $this->conexion->consulta("INSERT INTO localidad VALUES(NULL,'$nombre',$idpadre)");
        }
        else{
            $this->conexion->consulta("INSERT INTO localidad VALUES(NULL,'$nombre',NULL)");
        }
        $this->conexion->desconectar();
    }

    function editarCatOf($id, $nombre, $idpadre){
        $this->conexion->conectar();
        if($idpadre!=""){
            $this->conexion->consulta("UPDATE categoriaoficio set nombre='$nombre', idcategoriaoficio=$idpadre WHERE id=$id");
        }
        else{
            $this->conexion->consulta("UPDATE categoriaoficio set nombre='$nombre', idcategoriaoficio=NULL WHERE id=$id");
        }
        $this->conexion->desconectar();
    }

    function editarOf($id, $nombre, $idpadre){
        $this->conexion->conectar();
        if($idpadre!=""){
            $this->conexion->consulta("UPDATE oficio set nombre='$nombre', idcategoriaoficio=$idpadre WHERE id=$id");
        }
        else{
            $this->conexion->consulta("UPDATE oficio set nombre='$nombre', idcategoriaoficio=0 WHERE id=$id");
        }
        $this->conexion->desconectar();
    }

    function editarLoc($id, $nombre, $idpadre){
        $this->conexion->conectar();
        if($idpadre!=""){
            $this->conexion->consulta("UPDATE localidad set nombre='$nombre', idlocalidad=$idpadre WHERE id=$id");
        }
        else{
            $this->conexion->consulta("UPDATE localidad set nombre='$nombre', idlocalidad=NULL WHERE id=$id");
        }
        $this->conexion->desconectar();
    }

    function oficios(){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT * FROM categoriaoficio WHERE idcategoriaoficio IS NULL");
        $filas = array("padres"=>[], "subcategorias"=>[], "oficios"=>[]);
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("ID"=>$fila['id'], "nombre"=>$fila['nombre'], "padre"=>$fila['idcategoriaoficio']);
            array_push($filas['padres'],$f);
        }
        $consulta=$this->conexion->consulta("SELECT * FROM categoriaoficio WHERE idcategoriaoficio IS NOT NULL");
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("ID"=>$fila['id'], "nombre"=>$fila['nombre'], "padre"=>$fila['idcategoriaoficio']);
            array_push($filas['subcategorias'],$f);
        }
        $consulta=$this->conexion->consulta("SELECT * FROM oficio");
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("ID"=>$fila['id'], "nombre"=>$fila['nombre'], "padre"=>$fila['idcategoriaoficio']);
            array_push($filas['oficios'],$f);
        }
        $this->conexion->desconectar();
        return $filas;
    }

    function ciudades(){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT * FROM localidad WHERE id NOT IN (SELECT idlocalidad FROM localidad WHERE idlocalidad IS NOT NULL)");
        $this->conexion->desconectar();
        $filas = array();
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("ID"=>$fila['id'], "nombre"=>$fila['nombre']);
            array_push($filas,$f);
        }
        return $filas;
    }
}
?>