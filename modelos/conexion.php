<?php

class conexion{
    var $servername;
    var $database;
    var $username;
    var $password;
    var $conn;

    function __construct() {
        $this->servername = "localhost";
        $this->database = "topicos";
        $this->username = "root";
        $this->password = "";
    }

    public function conectar(){
        $this->conn = mysqli_connect($this->servername, $this->username, $this->password, $this->database);
        if (!$this->conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
    }

    function desconectar(){
        mysqli_close($this->conn);
    }

    function consulta($consulta){
        return mysqli_query ($this->conn,$consulta);
    }
}
?>