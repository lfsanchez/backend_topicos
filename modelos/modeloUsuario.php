<?php
include "conexion.php";

class modeloUsuario{

    var $conexion;

    function __construct()
    {
        $this->conexion = new conexion();
    }

    function buscarAdmin($email){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT usuario.*,persona.nombre,persona.apellidop,persona.apellidom FROM usuario, persona WHERE email='$email' AND estado='A' AND idpersona=ci AND idpersona IN (SELECT personaid FROM administrador)");
        $this->conexion->desconectar();
        $filas = array();
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("ID"=>$fila[0], "idpersona"=>$fila[4], "email"=>$fila[1], "password"=>$fila[3], "nombre"=>$fila[5]." ".$fila[6]." ".$fila[7]);
            array_push($filas,$f);
        }
        return $filas;
    }

    function buscarEmpleado($email){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT usuario.*,persona.nombre,persona.apellidop,persona.apellidom FROM usuario, persona WHERE email='$email' AND estado='A' AND idpersona=ci AND idpersona IN (SELECT cipersona FROM empleado)");
        $this->conexion->desconectar();
        $filas = array();
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("idpersona"=>$fila["idpersona"], "nombre"=>$fila["nombre"]." ".$fila["apellidop"]." ".$fila["apellidom"], "email"=>$fila["email"], "password"=>$fila["password"]);
            array_push($filas,$f);
        }
        return $filas;
    }

    function buscarEmpleador($email){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT usuario.*,persona.nombre,persona.apellidop,persona.apellidom FROM usuario, persona WHERE email='$email' AND estado='A' AND idpersona=ci AND idpersona IN (SELECT cipersona FROM empleador)");
        $this->conexion->desconectar();
        $filas = array();
        while($fila=mysqli_fetch_array($consulta)){
            $f = array("idpersona"=>$fila["idpersona"], "nombre"=>$fila["nombre"]." ".$fila["apellidop"]." ".$fila["apellidom"], "email"=>$fila["email"], "password"=>$fila["password"]);
            array_push($filas,$f);
        }
        return $filas;
    }

    function listar(){
        $this->conexion->conectar();
        $consulta1=$this->conexion->consulta("SELECT * FROM usuario");
        $filas = array();
        while($fila=mysqli_fetch_array($consulta1)){
            if(count($fila)>0){
                $consulta3=$this->conexion->consulta("SELECT * FROM empleado WHERE cipersona=$fila[4]");
                $consulta4=$this->conexion->consulta("SELECT * FROM empleador WHERE cipersona=$fila[4]");
                $consulta5=$this->conexion->consulta("SELECT * FROM persona WHERE ci=$fila[4]");
                $fila5=mysqli_fetch_array($consulta5);
                if($fila[2]=='A'){
                    $estado='Activo';
                }
                elseif($fila[2]=='B'){
                    $estado='Baja';
                }
                elseif($fila[2]=='R'){
                    $estado='Rechazado';
                }
                else{
                    $estado='Pendiente';
                }
                if($fila3=mysqli_fetch_array($consulta3)){
                    $f = array("ID"=>$fila5[3], "nombre"=>$fila5[0]." ".$fila5[1]." ".$fila5[2], "email"=>$fila[1], "estado"=>$estado, "fechar"=>$fila3[6]);
                }
                if($fila4=mysqli_fetch_array($consulta4)){
                    $f = array("ID"=>$fila5[3], "nombre"=>$fila5[0]." ".$fila5[1]." ".$fila5[2], "email"=>$fila[1], "estado"=>$estado, "fechar"=>$fila4[3]);
                }
                if(isset($f)){
                    array_push($filas,$f);
                }
            }
        }
        $this->conexion->desconectar();
        $array=["rows"=>$filas];
        return $array;
    }

    function registrarUsuario($ci, $nombre, $AP, $AM, $dir, $loc, $email, $pass, $FN, $FR, $telefono, $FP, $FA='', $FC='', $FRC='', $oficios = "", $empleado){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT * FROM persona WHERE ci=$ci");
        if($fila=mysqli_fetch_array($consulta)){
            if($empleado != "si"){
                $consulta=$this->conexion->consulta("SELECT * FROM empleador,usuario WHERE cipersona=$ci AND cipersona=idpersona AND email='$email'");
            }
            else{
                $consulta=$this->conexion->consulta("SELECT * FROM empleado,usuario WHERE cipersona=$ci AND cipersona=idpersona AND email='$email'");
            }
            if($fila=mysqli_fetch_array($consulta)){
                if($fila['estado']=="P"){
                    echo "pendiente";
                }
                elseif($fila['estado']=="B"){
                    echo "baja";
                }
                elseif($fila['estado']=="R") {
                    echo "rechazado";
                }
                else{
                    echo "cuenta";
                }
            }
            else{
                $consulta=$this->conexion->consulta("SELECT * FROM usuario WHERE idpersona=$ci AND email='$email'");
                $fila=mysqli_fetch_array($consulta);
                if(!isset($fila)){
                    $this->conexion->consulta("INSERT INTO usuario VALUES (NULL, '$email', 'P', '$pass', $ci)");
                }
                if($empleado != "si"){
                    $this->conexion->consulta("INSERT INTO empleador VALUES (0, '$FN', '$FP', '$FR', $telefono, $ci)");
                }
                else{
                    $this->conexion->consulta("INSERT INTO empleado VALUES (0, '$FN', '$FA', '$FC', '$FP', '$FRC', '$FR', $ci, $telefono)");
                    foreach ($oficios as $value) {
                        $this->conexion->consulta("INSERT INTO empleadooficio VALUES (NULL, '".$value->{'registro'}."', NULL, '".$value->{'HI'}."', '".$value->{'HF'}."', $ci, ".intval($value->{'ID'}).")");
                    }
                }
                echo 'ok';
            }
            echo 'persona';
        }
        else{
            $this->conexion->consulta("INSERT INTO persona VALUES ('$nombre', '$AP', '$AM', $ci, '$dir', $loc)");
            if($empleado != "si"){
                $this->conexion->consulta("INSERT INTO empleador VALUES (0, '$FN', '$FP', '$FR', $telefono, $ci)");
            }
            else{
                $this->conexion->consulta("INSERT INTO empleado VALUES (0, '$FN', '$FA', '$FC', '$FP', '$FRC', '$FR', $ci, $telefono)");
                foreach ($oficios as $value) {
                    $this->conexion->consulta("INSERT INTO empleadooficio VALUES (NULL, '".$value->{'registro'}."', NULL, '".$value->{'HI'}."', '".$value->{'HF'}."', $ci, ".intval($value->{'ID'}).")");
                }
            }
            $this->conexion->consulta("INSERT INTO usuario VALUES (NULL, '$email', 'P', '$pass', $ci)");
            $consulta=$this->conexion->consulta("SELECT fotoantecedente FROM empleado WHERE cipersona=$ci");
            $fila=mysqli_fetch_array($consulta);
            echo $fila['fotoantecedente'];
        }
        $this->conexion->desconectar();
    }

    function datos_personales($id){
        $this->conexion->conectar();
        $consulta=$this->conexion->consulta("SELECT * FROM persona WHERE ci=$id");
        $datos = array();
        if($fila=mysqli_fetch_array($consulta)){
            $datos['persona']=array("nombre"=>$fila['nombre'], "AP"=>$fila['apellidop'], "AM"=>$fila['apellidom'], "ci"=>$fila['ci']);
            $consulta=$this->conexion->consulta("SELECT * FROM localidad WHERE id=".$fila['idlocalidad']);
            $fila=mysqli_fetch_array($consulta);
            $datos['localidad']=array();
            while($fila['idlocalidad']!=null){
                array_push($datos['localidad'], $fila['nombre']);
                $consulta=$this->conexion->consulta("SELECT * FROM localidad WHERE id=".$fila['idlocalidad']);
                $fila=mysqli_fetch_array($consulta);
            }
            array_push($datos['localidad'], $fila['nombre']);
            $consulta=$this->conexion->consulta("SELECT * FROM usuario WHERE idpersona=$id");
            $fila=mysqli_fetch_array($consulta);
            if($fila['estado']=='A'){
                $estado='Activo';
            }
            elseif($fila['estado']=='B'){
                $estado='Baja';
            }
            elseif($fila['estado']=='P'){
                $estado='Pendiente';
            }
            else{
                $estado='Rechazado';
            }
            $datos['cuenta']=array('email'=>$fila['email'], 'estado'=>$estado);
            $consulta1=$this->conexion->consulta("SELECT * FROM empleado WHERE cipersona=$id");
            $consulta2=$this->conexion->consulta("SELECT * FROM empleador WHERE cipersona=$id");
            if($fila=mysqli_fetch_array($consulta1)){
                $datos['usuario']=array('tipo'=>'Empleado', 'CP'=>$fila['calificacionpromedio'],'FN'=>$fila['fnacimiento'],'FP'=>$fila['fotoperfil'], 'FR'=>$fila['fregistro'],'telefono'=>$fila['telefono'], 'FA'=>$fila['fotoantecedente'], 'FC'=>$fila['fotocarnet'], 'FRC'=>$fila['fotoreconoc']);
            }
            else{
                $fila=mysqli_fetch_array($consulta2);
                $datos['usuario']=array('tipo'=>'Empleador', 'CP'=>$fila['calificacionpromedio'],'FN'=>$fila['fnacimiento'],'FP'=>$fila['fotoperfil'], 'FR'=>$fila['fregistro'],'telefono'=>$fila['telefono'], 'FA'=>'', 'FC'=>'', 'FRC'=>'');
            }
            return $datos;
        }
        $this->conexion->desconectar();
    }

    function aprobar($id, $valor){
        $this->conexion->conectar();
        if($valor){
            $this->conexion->consulta("UPDATE usuario SET estado = 'A' WHERE idpersona = $id");
        }
        else{
            $this->conexion->consulta("UPDATE usuario SET estado = 'R' WHERE idpersona = $id");
        }
        $this->conexion->desconectar();
    }
}
?>