
<!DOCTYPE html>
<?php
if(!isset($_COOKIE["admin"])){
    header("location: http://192.168.1.160/topicos/login/");
}
?>
<html lang="es">
<head>
    <title>Usuarios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/icono.png" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>
    <link href="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
    <script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table-locale-all.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/extensions/export/bootstrap-table-export.min.js"></script>

</head>
<body>
<?php
include("../componentes/navbar.php");
?>
    <div class="content-page-container full-reset custom-scroll-containers">
        <?php
        include("../componentes/header.php");
        ?>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles"><center>Datos Maestros</center> </h1>
            </div>
        </div>
        <div class="container-fluid"  style="margin: 10px 0;">
            <center><table id="datos">
                <tr style="padding: 10px">
                    <td><label>Dato maestro </label></td>
                    <td><input id="iddato" value="" type="hidden"><input id="dato"></td>
                </tr>
                <tr>
                    <td><label>Tipo</label></td>
                    <td><select id="opcion" onchange="padre();">
                        <option></option>
                        <option value="1">Categoria Oficio</option>
                        <option value="2">Oficio</option>
                        <option value="3">Localidad</option>
                    </select></td>
                </tr>
                <tr>
                    <td><label>Padres</label></td>
                    <td><select id="padres">
                        <option value=""></option>
                    </select></td>
                </tr>
                <tr>
                    <td colspan="2"><center><input type="button" value="Guardar" onclick="guardar();"></center></td>
                </tr>
            </table></center>
        </div>
        <div id="toolbar">
            <center><label>Categoria</label>
                <select id="catfilter">
                <option></option>
                <option value="1">Categoria Oficio</option>
                <option value="2">Oficio</option>
                <option value="3">Localidad</option>
            </select></center>
        </div>
        <div>
            <center><table
                id="table"
                data-filter="#toolbar"
                data-minimum-count-columns="2"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, all]"
                data-url="http://192.168.1.160/topicos/controladores/APIDMController?opcion=lista"
                data-response-handler="responseHandler">
            </table></center>
        </div>
    </div>
</body>
<script>
    var $table = $('#table')
    var $categorias = $('#catfilter')
    var selections = []

    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.id
        })
    }

    function responseHandler(res) {
        $.each(res.rows, function (i, row) {
            row.state = $.inArray(row.id, selections) !== -1
        })
        return res
    }

    function detailFormatter(index, row) {
        var html = []
        $.each(row, function (key, value) {
            html.push('<p><b>' + key + ':</b> ' + value + '</p>')
        })
        return html.join('')
    }

    function operateFormatter(value, row, index) {
        return [
            '<a class="edit" href="javascript:void(0)" title="edit">',
            '<i class="fa fa-edit"></i>',
            '</a>  ',
            '<a class="remove" href="javascript:void(0)" title="Remove">',
            '<i class="fa fa-trash"></i>',
            '</a>'
        ].join('')
    }

    window.operateEvents = {
        'click .edit': function (e, value, row, index) {
            var id = $('#iddato');
            var dato = $('#dato');
            var opcion = $('#opcion');
            id.val(row.ID);
            dato.val(row.nombre);
            opcion.val(row.idcat);
            opcion.prop( "disabled", true );
            padre();
        },
        'click .remove': function (e, value, row, index) {
            var settings = {
                "url": "http://192.168.1.160/topicos/controladores/APIDMController",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                "data": {
                    "id": row.ID,
                    "idcat":row.idcat,
                    "opcion": "eliminar"
                }
            };
            $.ajax(settings).done();
            location.reload();
        }
    }

    function initTable() {
        $table.bootstrapTable('destroy').bootstrapTable({
            height: 550,
            locale: "es-MX",
            columns: [
                [{
                    field: 'nombre',
                    title: 'Nombre',
                    sortable: true,
                    align: 'center'
                }, {
                    field: 'categoria',
                    title: 'Categoria',
                    sortable: true,
                    align: 'center',
                }, {
                    field: 'padre',
                    title: 'Padre',
                    sortable: true,
                    align: 'center',
                },{
                    field: 'operate',
                    title: '',
                    align: 'center',
                    clickToSelect: false,
                    events: window.operateEvents,
                    formatter: operateFormatter,
                }]
            ]
        })

    }

    $(function() {
        $categorias.change(function () {
            if($categorias.val()==1){
                $table.bootstrapTable('filterBy', {idcat: [1]});
            }
            else{
                if($categorias.val()==2){
                    $table.bootstrapTable('filterBy', {idcat: [2]});
                }
                else{
                    if($categorias.val()==3){
                        $table.bootstrapTable('filterBy', {idcat: [3]});
                    }
                    else{
                        $table.bootstrapTable('filterBy', {});
                    }
                }
            }
        })
    })

    $(function() {
        initTable()

        $('#locale').change(initTable)
    })
</script>
<script>
    function padre(){
        var table = document.getElementById("datos");
        var op=$("#opcion").val();
        var settings = {
            "url": "http://192.168.1.160/topicos/controladores/APIDMController",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            "data": {
                "op": op,
                "opcion": "padres"
            }
        };

        $.ajax(settings).done(function (response) {
            var content = JSON.parse(response);
            if(Object.keys(content).length>0){
                var selectList = document.getElementById("padres");
                while(selectList.firstChild) {
                    selectList.removeChild(selectList.firstChild);
                }
                var option = document.createElement("option");
                option.setAttribute("value", "");
                option.text = "";
                selectList.appendChild(option);
                for (var j = 0; j < Object.keys(content).length; j++) {
                    var option = document.createElement("option");
                    option.setAttribute("value", content[j]['ID']);
                    option.text = content[j]['nombre'];
                    selectList.appendChild(option);
                }
            }
        });
    }
    function guardar(){
        var opcion=$("#opcion");
        opcion.prop( "disabled", false );
        var iddato=$("#iddato").val();
        var dato=$("#dato").val();
        var op=$("#opcion").val();
        var padres=$("#padres").val();
        var settings = {
            "url": "http://192.168.1.160/topicos/controladores/APIDMController",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            "data": {
                "iddato": iddato,
                "dato": dato,
                "idcat": op,
                "idpadre": padres,
                "opcion":"guardar"
            }
        };

        $.ajax(settings).done();
        location.reload();
    }
</script>
</html>