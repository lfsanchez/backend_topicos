
<!DOCTYPE html>
<?php
if(!isset($_COOKIE["admin"])){
    header("location: http://192.168.1.160/topicos/login/");
}
?>
<html lang="es">
<head>
    <title>Usuarios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="../assets/icons/icono.png" />
    <script src="../js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="../css/sweet-alert.css">
    <link rel="stylesheet" href="../css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="../js/modernizr.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/main.js"></script>
    <link href="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
    <script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table-locale-all.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/extensions/export/bootstrap-table-export.min.js"></script>

</head>
<body>
<?php
include("../componentes/navbar.php");
?>
    <div class="content-page-container full-reset custom-scroll-containers">
        <?php
        include("../componentes/header.php");
        ?>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles"><center>Usuarios</center> </h1>
            </div>
        </div>
        <div id="toolbar">
            <center><label>Estado</label>
            <select id="opcion">
                <option value=""></option>
                <option value="Activo">Activo</option>
                <option value="Baja">Baja</option>
                <option value="Pendiente">Pendiente</option>
                <option value="Rechazado">Rechazado</option>
            </select>
            <label>Fecha de Registro</label>
            <input id="fechareg" type="date">
            <input id="todof" type="button" value="Todos los registros"></center>
        </div>
        <div>
            <center><table
                id="table"
                data-filter="#toolbar"
                data-minimum-count-columns="2"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, all]"
                data-url="http://192.168.1.160/topicos/controladores/APIUsuarioController?opcion=lista"
                data-response-handler="responseHandler">
            </table></center>
        </div>
    </div>
</body>
<script>
    var $table = $('#table')
    var $estado = $('#opcion')
    var $fecha = $('#fechareg')
    var $tf = $('#todof')
    var selections = []

    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.id
        })
    }

    function responseHandler(res) {
        $.each(res.rows, function (i, row) {
            row.state = $.inArray(row.id, selections) !== -1
        })
        return res
    }

    function dateFormatter(index, row) {
        var html = []
        $.each(row, function (key, value) {
            if(key==='fechar'){
                var d=Date.parse(value);
                d=d+4*3600*1000;
                d= new Date(d);
                var mes = d.getMonth() + 1;
                html.push(d.getDay().toLocaleString('en-US', {
                    minimumIntegerDigits: 2,
                    useGrouping: false
                })+"/"+mes.toLocaleString('en-US', {
                    minimumIntegerDigits: 2,
                    useGrouping: false
                })+"/"+d.getFullYear());
            }
        })
        return html.join('')
    }

    function operateFormatter(value, row, index) {
        return [
            '<a class="edit" href="../datos_personales?id='+row.ID+'" title="edit">',
            'Ver',
            '</a>  ',
        ].join('')
    }

    function initTable() {
        $table.bootstrapTable('destroy').bootstrapTable({
            height: 550,
            locale: "es-MX",
            columns: [
                [{
                    field: 'ID',
                    title: 'CI',
                    sortable: true,
                    align: 'center'
                }, {
                    field: 'nombre',
                    title: 'Nombre',
                    sortable: true,
                    align: 'center',
                }, {
                    field: 'estado',
                    title: 'Estado',
                    sortable: true,
                    align: 'center',
                },{
                    field: 'fechar',
                    title: 'Fecha de Registro',
                    sortable: true,
                    align: 'center',
                    formatter: dateFormatter,
                },{
                    field: 'operate',
                    title: '',
                    align: 'center',
                    clickToSelect: false,
                    formatter: operateFormatter,
                }]
            ]
        })

    }

    $(function() {
        $estado.change(function () {
            if($estado.val()!=""){
                if($fecha.val()!=""){
                    var d= Date.parse($fecha.val());
                    d=d+4*3600*1000;
                    d=new Date(d);
                    var mes =d.getMonth()+1;
                    $table.bootstrapTable('filterBy', {estado: [$estado.val()],fechar: [d.getFullYear()+"-"+mes.toLocaleString('en-US', {
                            minimumIntegerDigits: 2,
                            useGrouping: false
                        })+"-"+d.getDay().toLocaleString('en-US', {
                            minimumIntegerDigits: 2,
                            useGrouping: false
                        })]});
                }
                else{
                    $table.bootstrapTable('filterBy', {estado: [$estado.val()]});
                }
            }
            else {
                if ($fecha.val() != "") {
                    var d = Date.parse($fecha.val());
                    d = d + 4 * 3600 * 1000;
                    d = new Date(d);
                    var mes = d.getMonth() + 1;
                    $table.bootstrapTable('filterBy', {
                        fechar: [d.getFullYear() + "-" + mes.toLocaleString('en-US', {
                            minimumIntegerDigits: 2,
                            useGrouping: false
                        }) + "-" + d.getDay().toLocaleString('en-US', {
                            minimumIntegerDigits: 2,
                            useGrouping: false
                        })]
                    });
                }
            }
        })
    })

    $(function() {
        $fecha.change(function () {
            var d= Date.parse($fecha.val());
            d=d+4*3600*1000;
            d=new Date(d);
            var mes =d.getMonth()+1;
            if($estado.val()!=""){
                $table.bootstrapTable('filterBy', {estado: [$estado.val()],fechar: [d.getFullYear()+"-"+mes.toLocaleString('en-US', {
                        minimumIntegerDigits: 2,
                        useGrouping: false
                    })+"-"+d.getDay().toLocaleString('en-US', {
                        minimumIntegerDigits: 2,
                        useGrouping: false
                    })]});
            }
            else{
                $table.bootstrapTable('filterBy', {fechar: [d.getFullYear()+"-"+mes.toLocaleString('en-US', {
                        minimumIntegerDigits: 2,
                        useGrouping: false
                    })+"-"+d.getDay().toLocaleString('en-US', {
                        minimumIntegerDigits: 2,
                        useGrouping: false
                    })]});
            }
        })
    })

    $(function() {
        $tf.click(function () {
            $fecha.val("");
            $estado.val("");
            $table.bootstrapTable('filterBy', {});
        })
    })

    $(function() {
        initTable()

        $('#locale').change(initTable)
    })
</script>
<script>
    function padre(){
        var table = document.getElementById("datos");
        var op=$("#opcion").val();
        var settings = {
            "url": "http://192.168.1.160/topicos/controladores/APIDMController",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            "data": {
                "op": op,
                "opcion": "padres"
            }
        };

        $.ajax(settings).done(function (response) {
            var content = JSON.parse(response);
            if(Object.keys(content).length>0){
                var selectList = document.getElementById("padres");
                for (var j = 0; j < Object.keys(content).length; j++) {
                    var option = document.createElement("option");
                    option.setAttribute("value", content[j]['ID']);
                    option.text = content[j]['nombre'];
                    selectList.appendChild(option);
                }
            }
        });
    }
    function guardar(){
        var opcion=$("#opcion");
        opcion.prop( "disabled", false );
        var iddato=$("#iddato").val();
        var dato=$("#dato").val();
        var op=$("#opcion").val();
        var padres=$("#padres").val();
        var settings = {
            "url": "http://192.168.1.160/topicos/controladores/APIDMController",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            "data": {
                "iddato": iddato,
                "dato": dato,
                "idcat": op,
                "idpadre": padres,
                "opcion":"guardar"
            }
        };

        $.ajax(settings).done();
        location.reload();
    }
</script>
</html>