<?php
include "../modelos/modeloDM.php";

$modelo = new modeloDM();
if(isset($_POST['opcion'])){
    if($_POST['opcion']=="padres"){
        if($_POST['op']==1){
            echo json_encode($modelo->buscarCatOf());
        }
        elseif($_POST['op']==2){
            echo json_encode($modelo->buscarOf());
        }
        else{
            echo json_encode($modelo->buscarLoc());
        }
    }
    if($_POST['opcion']=="eliminar"){
        if($_POST['idcat']==1){
            $modelo->eliminarCatOf($_POST['id']);
        }
        elseif($_POST['idcat']==2){
            $modelo->eliminarOf($_POST['id']);
        }
        else{
            $modelo->eliminarLoc($_POST['id']);
        }
    }
    if($_POST['opcion']=="guardar"){
        if($_POST['idcat']==1){
            if($_POST['iddato']!=""){
                $modelo->editarCatOf($_POST['iddato'], $_POST['dato'], $_POST['idpadre']);
            }
            else{
                $modelo->guardarCatOf($_POST['dato'], $_POST['idpadre']);
            }
        }
        elseif($_POST['idcat']==2){
            if($_POST['iddato']!=""){
                $modelo->editarOf($_POST['iddato'], $_POST['dato'], $_POST['idpadre']);
            }
            else{
                $modelo->guardarOf($_POST['dato'], $_POST['idpadre']);
            }
        }
        else{
            if($_POST['iddato']!=""){
                $modelo->editarLoc($_POST['iddato'], $_POST['dato'], $_POST['idpadre']);
            }
            else{
                $modelo->guardarLoc($_POST['dato'], $_POST['idpadre']);
            }
        }
    }
    if($_POST['opcion']=="ciudades"){
        echo json_encode($modelo->ciudades());
    }
    if($_POST['opcion']=="oficios"){
        echo json_encode($modelo->oficios());
    }
}
else{
    if($_GET['opcion']=='lista'){
        $array = $modelo->Listar();
        $array["total"]=count($array);
        $array["totalNotFiltered"]=$array["total"];
        echo json_encode($array);
    }
}

?>