<?php
include "../modelos/modeloUsuario.php";

$modelo = new modeloUsuario();

if(isset($_POST['opcion'])){
    if($_POST['opcion']=="loginAdmin"){
        $admin = $modelo->buscarAdmin($_POST['email']);
        if(count($admin)>0){
            if(password_verify ( $_POST['pass'] , $admin[0]["password"])){
                echo json_encode(array("ID"=>$admin[0]["idpersona"], "nombre"=>$admin[0]["nombre"]));
            }
            else{
                echo "password";
            }
        }
        else{
            echo "usuario";
        }
    }
    if($_POST['opcion']=="Login"){
        if($_POST['empleado']=="si"){
            $admin = $modelo->buscarEmpleado($_POST['email']);
            if(count($admin)>0){
                if(password_verify ( $_POST['pass'] , $admin[0]["password"])){
                    echo json_encode(array("ID"=>$admin[0]["idpersona"], "nombre"=>$admin[0]["nombre"]));
                }
                else{
                    echo "password";
                }
            }
            else{
                echo "usuario";
            }
        }
        else{
            $admin = $modelo->buscarEmpleador($_POST['email']);
            if(count($admin)>0){
                if(password_verify ( $_POST['pass'] , $admin[0]["password"])){
                    echo json_encode(array("ID"=>$admin[0]["idpersona"], "nombre"=>$admin[0]["nombre"]));
                }
                else{
                    echo "password";
                }
            }
            else{
                echo "usuario";
            }
        }
    }
    if($_POST['opcion']=="RegistrarUsuario"){
        $archivo = base64_encode(file_get_contents($_FILES['FP']['tmp_name']));
        $codex=password_hash($_POST['pass'], PASSWORD_DEFAULT);
        if($_POST['empleado']=="si"){
            $FA = base64_encode(file_get_contents($_FILES['FA']['tmp_name']));
            $FC = base64_encode(file_get_contents($_FILES['FC']['tmp_name']));
            $FRC = base64_encode(file_get_contents($_FILES['FRC']['tmp_name']));
            $registro=$modelo->registrarUsuario($_POST['ci'], $_POST['nombre'], $_POST['AP'], $_POST['AM'], $_POST['dir'], $_POST['loc'], $_POST['email'], $codex, $_POST['FN'], $_POST['FR'], $_POST['telefono'], $archivo, $FA, $FC, $FRC, json_decode($_POST['oficios']),$_POST['empleado']);
        }
        else{
            $registro=$modelo->registrarUsuario($_POST['ci'], $_POST['nombre'], $_POST['AP'], $_POST['AM'], $_POST['dir'], $_POST['loc'], $_POST['email'], $codex, $_POST['FN'], $_POST['FR'], $_POST['telefono'], $archivo,"","","","",$_POST['empleado']);
        }
    }

    if($_POST['opcion']=="datos_personales"){
        echo json_encode($modelo->datos_personales($_POST['id']));
    }

    if($_POST['opcion']=="aprobar"){
        if($_POST['aceptado']=="true"){
            $modelo->aprobar($_POST['id'], true);
        }
        else{
            $modelo->aprobar($_POST['id'], false);
        }
    }
}
else{
    if($_GET['opcion']=='lista'){
        $array = $modelo->Listar();
        $array["total"]=count($array["rows"]);
        $array["totalNotFiltered"]=$array["total"];
        echo json_encode($array);
    }
}

?>